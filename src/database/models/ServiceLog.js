import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const ServiceLog = sequelize.define(
  'service_log',
  {
    id: {
      type: Sequelize.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    request: { type: Sequelize.TEXT('long'), field: 'request' },
    response: { type: Sequelize.TEXT, field: 'response', allowNull: true },
    service: { type: Sequelize.STRING(191), field: 'service' },
    user_id: {
      type: Sequelize.INTEGER.UNSIGNED,
      field: 'user_id',
      allowNull: true,
    },
    exception: { type: Sequelize.TEXT, field: 'exception', allowNull: true },
    calixta_request: {
      type: Sequelize.TEXT,
      field: 'calixta_request',
      allowNull: true,
    },
    calixta_response: {
      type: Sequelize.TEXT,
      field: 'calixta_response',
      allowNull: true,
    },
    created_at: { type: Sequelize.DATE, field: 'created_at', allowNull: true },
    updated_at: { type: Sequelize.DATE, field: 'updated_at', allowNull: true },
  },
  { underscored: true, timestamps: false }
)

export default ServiceLog
