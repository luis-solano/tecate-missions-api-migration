import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import moment from 'moment-timezone'

const Segment = sequelize.define(
  'segments',
  {
    name: { type: Sequelize.STRING(100) },
    description: { type: Sequelize.STRING(191), allowNull: true },
    role_id: { type: Sequelize.INTEGER, allowNull: true },
    region_id: { type: Sequelize.INTEGER, allowNull: true },
    type_segment_id: { type: Sequelize.INTEGER, allowNull: true },
    created_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('created_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('updated_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
  },
  { underscored: true, timestamps: false }
)

export default Segment
