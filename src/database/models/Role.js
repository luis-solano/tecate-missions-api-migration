import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import moment from 'moment-timezone'

const Role = sequelize.define(
  'roles',
  {
    name: { type: Sequelize.STRING, unique: true, field: 'name' },
    slug: { type: Sequelize.STRING, unique: true, field: 'slug' },
    description: { type: Sequelize.TEXT, unique: true, field: 'description' },
    created_at: {
      type: Sequelize.DATE,
      field: 'created_at',
      allowNull: true,
      get: function () {
        return moment(this.getDataValue('created_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
    updated_at: {
      type: Sequelize.DATE,
      field: 'updated_at',
      allowNull: true,
      get: function () {
        return moment(this.getDataValue('updated_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
  },
  { underscored: true, timestamps: false }
)

export default Role
