import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const SegmentSurvey = sequelize.define(
  'segment_survey',
  {
    segment_id: { type: Sequelize.INTEGER.UNSIGNED },
    survey_id: { type: Sequelize.INTEGER.UNSIGNED },
  },
  { underscored: true, timestamps: false, tableName: 'segment_survey' }
)

export default SegmentSurvey
