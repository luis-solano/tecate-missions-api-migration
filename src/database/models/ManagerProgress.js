import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const ManagerProgress = sequelize.define(
  'manager_progress',
  {
    user_id: { type: Sequelize.INTEGER.UNSIGNED },
    month: { type: Sequelize.STRING(191) },
    year: { type: Sequelize.STRING(191) },
    percent: { type: Sequelize.STRING(191) },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  {
    underscored: true,
    timestamps: false,
    freezeTableName: true,
  }
)

export default ManagerProgress
