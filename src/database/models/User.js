import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const User = sequelize.define(
  'users',
  {
    username: { type: Sequelize.STRING, field: 'username' },
    first_name: { type: Sequelize.STRING, field: 'first_name' },
    last_name: { type: Sequelize.STRING, field: 'last_name' },
    gender: { type: Sequelize.STRING, field: 'gender' },
    created_at: { type: Sequelize.DATE, field: 'created_at', allowNull: true },
    updated_at: { type: Sequelize.DATE, field: 'updated_at', allowNull: true },
  },
  { underscored: true, timestamps: false }
)

export default User
