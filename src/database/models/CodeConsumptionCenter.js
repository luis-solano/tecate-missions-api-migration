import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const CodeConsumptionCenter = sequelize.define(
  'code_consumption_centers',
  {
    code_id: { type: Sequelize.INTEGER.UNSIGNED },
    consumption_center_id: { type: Sequelize.INTEGER.UNSIGNED },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  { underscored: true, timestamps: false }
)

export default CodeConsumptionCenter
