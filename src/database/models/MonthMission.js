import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const MonthMission = sequelize.define(
  'month_missions',
  {
    missions_number: { type: Sequelize.INTEGER },
    month: { type: Sequelize.STRING(191) },
    month_name: { type: Sequelize.STRING(191) },
    year: { type: Sequelize.STRING(191) },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  { underscored: true, timestamps: false }
)

export default MonthMission
