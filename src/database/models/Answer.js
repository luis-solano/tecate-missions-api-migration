import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import moment from 'moment-timezone'

const Answer = sequelize.define(
  'answers',
  {
    answer: { type: Sequelize.TEXT, field: 'answer' },
    question_id: { type: Sequelize.INTEGER, field: 'question_id' },
    is_correct: { type: Sequelize.INTEGER, field: 'is_correct' },
    created_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('created_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('updated_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
  },
  { underscored: true, timestamps: false }
)

export default Answer
