import Sequelize from 'sequelize'
import moment from 'moment-timezone'
import { sequelize } from '../settings'
import Role from './Role'

const Mission = sequelize.define(
  'missions',
  {
    name: { type: Sequelize.STRING(100) },
    folio: { type: Sequelize.STRING(50) },
    consumption_center_id: { type: Sequelize.INTEGER.UNSIGNED },
    week_number: { type: Sequelize.SMALLINT },
    date_start: {
      type: Sequelize.DATE,
      get: function () {
        // or use get(){ }
        return {
          date: moment(this.getDataValue('date_start')).format(
            'YYYY-MM-DD HH:mm:ss'
          ),
          timezone_type: 3,
          timezone: 'America/Mexico_City',
        }
      },
    },
    date_finish: {
      type: Sequelize.DATE,
      get: function () {
        // or use get(){ }
        return {
          date: moment(this.getDataValue('date_finish')).format(
            'YYYY-MM-DD HH:mm:ss'
          ),
          timezone_type: 3,
          timezone: 'America/Mexico_City',
        }
      },
    },
    description: { type: Sequelize.TEXT },
    restrictions: { type: Sequelize.TEXT },
    term_conditions: { type: Sequelize.TEXT },
    role_id: { type: Sequelize.INTEGER.UNSIGNED },
    max_attachments: { type: Sequelize.SMALLINT },
    tutorial_url: { type: Sequelize.STRING, allowNull: true },
    days: { type: Sequelize.STRING },
    active: { type: Sequelize.BOOLEAN },
    slug: { type: Sequelize.STRING, unique: true, allowNull: true },
    points: { type: Sequelize.INTEGER.UNSIGNED, allowNull: true },
    created_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get: function () {
        return moment(this.getDataValue('created_at')).format(
          'YYYY-MM-DD HH:mm:ss'
        )
      },
      set: function () {
        this.setDataValue(
          'created_at',
          moment().tz('UTC').format('YYYY-MM-DD HH:mm:ss')
        )
      },
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get: function () {
        return moment(this.getDataValue('created_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
      set: function () {
        this.setDataValue(
          'updated_at',
          moment().tz('UTC').format('YYYY-MM-DD HH:mm:ss')
        )
      },
    },
    segment_id: { type: Sequelize.STRING(50), allowNull: true },
    type: { type: Sequelize.TEXT },
    survey_id: { type: Sequelize.INTEGER(10).UNSIGNED },
    special: { type: Sequelize.BOOLEAN },
    is_scheduled: { type: Sequelize.BOOLEAN },
    start_time: { type: Sequelize.TIME },
    time_to_end: { type: Sequelize.TIME },
    end_date: {
      type: Sequelize.DATE,
      get: function () {
        return (this.getDataValue('end_date') == null)
        ? null
        : moment(this.getDataValue('end_date')).format(
            'YYYY-MM-DD HH:mm:ss'
          )
      },
    },
    type_mission_id: { type: Sequelize.INTEGER(10).UNSIGNED },
  },
  { underscored: true, timestamps: false }
)

export default Mission
