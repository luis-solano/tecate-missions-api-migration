import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const UserTransaction = sequelize.define(
  'user_transactions',
  {
    user_id: { type: Sequelize.INTEGER.UNSIGNED },
    amount: { type: Sequelize.INTEGER },
    description: { type: Sequelize.STRING(191) },
    reference: { type: Sequelize.TEXT },
    created_at: { type: Sequelize.STRING, allowNull: true },
    applied_at: { type: Sequelize.STRING, allowNull: true },
  },
  { underscored: true, timestamps: false }
)

export default UserTransaction
