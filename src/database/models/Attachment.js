import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import moment from 'moment-timezone'

const Attachment = sequelize.define(
  'attachments',
  {
    mission_user_id: { type: Sequelize.INTEGER.UNSIGNED },
    original: { type: Sequelize.STRING },
    filename: { type: Sequelize.STRING },
    extension: { type: Sequelize.STRING },
    mime: { type: Sequelize.STRING, field: 'mime' },
    latitude: { type: Sequelize.DECIMAL(9, 6) },
    longitude: { type: Sequelize.DECIMAL(9, 6) },
    status: { type: Sequelize.ENUM('passed', 'pending', 'failed') },
    commentary: { type: Sequelize.TEXT, allowNull: true },
    created_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get: function () {
        return moment(this.getDataValue('created_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
      set: function () {
        this.setDataValue(
          'created_at',
          moment().tz('UTC').format('YYYY-MM-DD HH:mm:ss')
        )
      },
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get: function () {
        return moment(this.getDataValue('updated_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
      set: function () {
        this.setDataValue(
          'updated_at',
          moment().tz('UTC').format('YYYY-MM-DD HH:mm:ss')
        )
      },
    },
  },
  { underscored: true, timestamps: false }
)

export default Attachment
