import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const AdviserProgress = sequelize.define(
  'adviser_progress',
  {
    adviser_id: { type: Sequelize.INTEGER.UNSIGNED },
    mission_id: { type: Sequelize.INTEGER.UNSIGNED },
    percent: { type: Sequelize.STRING(191) },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  {
    underscored: true,
    timestamps: false,
    freezeTableName: true,
  }
)

export default AdviserProgress
