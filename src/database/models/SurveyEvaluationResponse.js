import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const SurveyEvaluationResponse = sequelize.define(
  'survey_evaluation_response',
  {
    question: { type: Sequelize.TEXT },
    answer: { type: Sequelize.TEXT },
    points: { type: Sequelize.DECIMAL },
    survey_evaluation_id: { type: Sequelize.INTEGER.UNSIGNED },
    is_correct: { type: Sequelize.INTEGER },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  { underscored: true, timestamps: false }
)

export default SurveyEvaluationResponse
