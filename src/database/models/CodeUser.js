import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import Mission from './Mission'
import Code from './Code'
import User from './User'

const CodeUser = sequelize.define(
  'CodeUser',
  {
    userId: {
      type: Sequelize.DataTypes.INTEGER,
      field: 'user_id',
    },
    codeId: {
      type: Sequelize.DataTypes.INTEGER,
      field: 'code_id',
    },
  },
  {
    tableName: 'code_user',
    underscored: true,
    timestamps: false,
  }
)

User.belongsToMany(Code, { through: CodeUser })
Code.belongsToMany(User, { through: CodeUser })

CodeUser.belongsTo(User)
CodeUser.belongsTo(Code)

export default CodeUser
