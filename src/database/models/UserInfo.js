import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import User from './User'

const UserInfo = sequelize.define(
  'UserInfo',
  {
    userId: { type: Sequelize.INTEGER, field: 'user_id' },
    tutorial_completed: {
      type: Sequelize.INTEGER,
      field: 'tutorial_completed',
    },
  },
  {
    tableName: 'user_info',
    underscored: true,
  }
)

UserInfo.belongsTo(User)

export default UserInfo
