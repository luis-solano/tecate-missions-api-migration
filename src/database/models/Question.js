import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import Answer from './Answer'
import moment from 'moment-timezone'

const Question = sequelize.define(
  'question',
  {
    question: { type: Sequelize.TEXT, field: 'question' },
    required: {
      type: Sequelize.INTEGER,
      field: 'required',
      get() {
        return this.getDataValue('required') ? 1 : 0
      },
    },
    type: { type: Sequelize.STRING, field: 'type' },
    survey_id: { type: Sequelize.INTEGER, field: 'survey_id' },
    created_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('created_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('updated_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
  },
  { underscored: true, timestamps: false }
)

Question.hasMany(Answer, { foreignKey: 'question_id' })

export default Question
