import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const MissionTransaction = sequelize.define(
  'mission_transactions',
  {
    mission_id: { type: Sequelize.DataTypes.INTEGER },
    user_mission_id: { type: Sequelize.DataTypes.INTEGER },
    user_transaction_id: { type: Sequelize.DataTypes.INTEGER },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  { underscore: true, timestamps: false }
)

export default MissionTransaction
