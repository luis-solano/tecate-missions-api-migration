import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import User from './User'

const Code = sequelize.define(
  'codes',
  {
    code: { type: Sequelize.STRING(10) },
    available: { type: Sequelize.BOOLEAN },
    role_id: { type: Sequelize.INTEGER.UNSIGNED },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
    consumption_center_id: {
      type: Sequelize.INTEGER.UNSIGNED,
      allowNull: true,
    },
    user_id: { type: Sequelize.INTEGER.UNSIGNED, allowNull: true },
    gived: { type: Sequelize.INTEGER, allowNull: true },
    gived_user: { type: Sequelize.STRING(191), allowNull: true },
  },
  { underscored: true, timestamps: false }
)

Code.belongsTo(User, { foreignKey: 'user_id' })

export default Code
