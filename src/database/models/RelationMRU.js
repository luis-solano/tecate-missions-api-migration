import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import Mission from './Mission'
import Role from './Role'
import User from './User'
import TutorialImages from './TutorialImages'
import MissionUser from './MissionUser'

const MissionRole = sequelize.define(
  'pivot',
  {
    roleId: {
      type: Sequelize.DataTypes.INTEGER,
      field: 'role_id',
    },
    missionId: {
      type: Sequelize.DataTypes.INTEGER,
      field: 'mission_id',
    },
  },
  {
    tableName: 'mission_role',
    underscored: true,
    timestamps: false,
  }
)

const UserRole = sequelize.define(
  'user_role',
  {
    role_id: {
      type: Sequelize.DataTypes.INTEGER,
      field: 'role_id',
    },
    user_id: {
      type: Sequelize.DataTypes.INTEGER,
      field: 'user_id',
    },
  },
  {
    tableName: 'role_user',
    underscored: true,
    timestamps: false,
  }
)
Role.belongsToMany(User, { through: UserRole })
User.belongsToMany(Role, { through: UserRole })

Role.belongsToMany(Mission, { through: MissionRole })
Mission.belongsToMany(Role, { through: MissionRole })

Mission.hasMany(MissionUser)
Mission.hasMany(TutorialImages)

export { Mission, Role, User, TutorialImages }
