import { Sequelize } from 'sequelize'
import { sequelize } from '../settings'
import { Question, Segment, SegmentSurvey } from './index'
import moment from 'moment-timezone'

const Survey = sequelize.define(
  'surveys',
  {
    name: { type: Sequelize.STRING(200) },
    points: { type: Sequelize.INTEGER },
    active: {
      type: Sequelize.INTEGER,
      get() {
        return this.getDataValue('active') ? 1 : 0
      },
    },
    segment_id: { type: Sequelize.INTEGER.UNSIGNED, allowNull: true },
    percentage: { type: Sequelize.INTEGER, allowNull: true },
    description: { type: Sequelize.TEXT },
    created_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('created_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('updated_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
    },
  },
  { underscored: true, timestamps: false }
)

Survey.hasMany(Question, { foreignKey: 'survey_id' })
Survey.belongsToMany(Segment, { through: SegmentSurvey })
Segment.belongsToMany(Survey, { through: SegmentSurvey })

export default Survey
