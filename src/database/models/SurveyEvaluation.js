import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const SurveyEvaluation = sequelize.define(
  'survey_evaluations',
  {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: Sequelize.STRING(200) },
    type: { type: Sequelize.STRING(200) },
    points: { type: Sequelize.DECIMAL },
    user_id: { type: Sequelize.INTEGER.UNSIGNED },
    survey_id: { type: Sequelize.INTEGER.UNSIGNED },
    mission_id: { type: Sequelize.INTEGER.UNSIGNED, allowNull: true },
    training_id: { type: Sequelize.INTEGER.UNSIGNED, allowNull: true },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  { underscored: true, timestamps: false }
)

export default SurveyEvaluation
