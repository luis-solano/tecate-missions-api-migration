import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const MissionFakeTransaction = sequelize.define(
  'mission_fake_transactions',
  {
    mission_id: { type: Sequelize.INTEGER.UNSIGNED },
    cdc_id: { type: Sequelize.INTEGER.UNSIGNED },
    user_mission_id: { type: Sequelize.INTEGER.UNSIGNED },
    points: { type: Sequelize.DECIMAL },
    user_transaction_id: { type: Sequelize.INTEGER },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  { underscore: true, timestamps: false }
)

export default MissionFakeTransaction
