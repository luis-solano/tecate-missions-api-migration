import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const ConsumptionCenter = sequelize.define(
  'consumption_centers',
  {
    name: { type: Sequelize.TEXT },
    user_id: { type: Sequelize.INTEGER.UNSIGNED },
    region: { type: Sequelize.TEXT },
    status: { type: Sequelize.INTEGER },
    plaza: { type: Sequelize.STRING(191), allowNull: true },
    plaza_name: { type: Sequelize.STRING(191), allowNull: true },
    subplaza: { type: Sequelize.STRING(191), allowNull: true },
    store_name: { type: Sequelize.STRING(191), allowNull: true },
    cr: { type: Sequelize.STRING(191), allowNull: true },
    district: { type: Sequelize.STRING(191), allowNull: true },
    cedis: { type: Sequelize.STRING(191), allowNull: true },
    cedis_name: { type: Sequelize.STRING(191), allowNull: true },
    gz: { type: Sequelize.STRING(191), allowNull: true },
    dr: { type: Sequelize.STRING(191), allowNull: true },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  { underscored: true, timestamps: false }
)

export default ConsumptionCenter
