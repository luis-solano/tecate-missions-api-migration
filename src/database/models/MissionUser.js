import Sequelize from 'sequelize'
import { sequelize } from '../settings'
import Mission from './Mission'
import Attachment from './Attachment'
import moment from 'moment-timezone'

const MissionUser = sequelize.define(
  'mission_user',
  {
    mission_id: { type: Sequelize.DataTypes.INTEGER },
    user_id: { type: Sequelize.DataTypes.INTEGER },
    cdc_id: { type: Sequelize.INTEGER },
    accept_term_conditions: { type: Sequelize.INTEGER },
    status: { type: Sequelize.STRING },
    date_evaluation: {
      type: Sequelize.DATE,
      allowNull: true,
      get: function () {
        // or use get(){ }
        return {
          date: moment(this.getDataValue('date_evaluation')).format(
            'YYYY-MM-DD hh:mm:ss'
          ),
          timezone_type: 3,
          timezone: 'America/Mexico_City',
        }
      },
    },
    created_at: {
      type: Sequelize.STRING,
      allowNull: true,
      get: function () {
        return moment(this.getDataValue('created_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
      set: function () {
        this.setDataValue(
          'created_at',
          moment().tz('UTC').format('YYYY-MM-DD HH:mm:ss')
        )
      },
    },
    updated_at: {
      type: Sequelize.STRING,
      allowNull: true,
      get: function () {
        return moment(this.getDataValue('updated_at')).format(
          'YYYY-MM-DD hh:mm:ss'
        )
      },
      set: function () {
        this.setDataValue(
          'updated_at',
          moment().tz('UTC').format('YYYY-MM-DD HH:mm:ss')
        )
      },
    },
  },
  {
    tableName: 'mission_user',
    underscored: true,
    timestamps: false,
  }
)

MissionUser.belongsTo(Mission, { foreignKey: 'mission_id' })
MissionUser.hasMany(Attachment, { foreignKey: 'mission_user_id' })

export default MissionUser
