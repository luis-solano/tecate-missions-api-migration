import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const MisionAdviserTransaction = sequelize.define(
  'mission_adviser_transactions',
  {
    mission_id: { type: Sequelize.INTEGER.UNSIGNED },
    user_id: { type: Sequelize.INTEGER.UNSIGNED },
    user_transaction_id: { type: Sequelize.INTEGER.UNSIGNED },
    created_at: { type: Sequelize.STRING, allowNull: true },
    updated_at: { type: Sequelize.STRING, allowNull: true },
  },
  { underscored: true, timestamps: false }
)

export default MisionAdviserTransaction
