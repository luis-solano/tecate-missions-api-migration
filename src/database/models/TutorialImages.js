import Sequelize from 'sequelize'
import { sequelize } from '../settings'

const TutorialImages = sequelize.define(
  'tutorial_images',
  {
    image_original: { type: Sequelize.STRING, field: 'image_original' },
    image_filename: { type: Sequelize.STRING, field: 'image_filename' },
    image_extension: { type: Sequelize.STRING, field: 'image_extension' },
    image_mime: { type: Sequelize.STRING, field: 'image_mime' },
    type_image_id: { type: Sequelize.STRING, field: 'type_image_id' },
  },
  { underscored: true, timestamps: false }
)

export default TutorialImages
