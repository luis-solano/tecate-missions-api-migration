import commonMiddleware from '../libs/commonMiddleware'
import { ServiceLog } from '../database/models'
import MissionService from '../libs/MissionService'
import SurveyService from '../libs/SurveyService'
import { sequelize } from '../database/settings'

const sendSurvey = async (event) => {
  if (
    typeof event.body.evaluation == 'undefined' &&
    !Array.isArray(event.body.evaluation)
  ) {
    return {
      body: {
        status: 'failed',
        data: {
          message: "Falta 'evaluation' como array",
        },
      },
      statusCode: 500,
    }
  }

  const dbTransaction = await sequelize.transaction()

  try {
    const surveyId = parseInt(event.pathParameters.survey_id)
    const evaluation = event.body.evaluation
    const missionId = event.body.mission_id ?? 0
    const trainingId = event.body.training_id ?? 0

    const serviceLog = await ServiceLog.create({
      request: JSON.stringify(event.body),
      service: 'surveys/' + surveyId + '/evaluate',
      user_id: event.sessionUser.id,
    })

    await MissionService.closeMission(
      missionId,
      event.sessionUser.id,
      dbTransaction
    )

    let response = await SurveyService.addEvaluation(
      event.sessionUser.id,
      surveyId,
      evaluation,
      missionId,
      trainingId,
      dbTransaction
    )

    dbTransaction.commit()

    serviceLog.response = response
    await serviceLog.save()

    return {
      body: {
        status: 'success',
        data: response,
      },
      statusCode: 200,
    }
  } catch (error) {
    dbTransaction.rollback()

    return {
      body: {
        status: 'failed',
        data: {
          message: error.message,
          stack: error.stack,
        },
      },
      statusCode: error.code ?? 500,
    }
  }
}

export const handler = commonMiddleware(sendSurvey)
