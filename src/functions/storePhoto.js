import moment from 'moment-timezone'
import commonMiddleware from '../libs/commonMiddleware'
import {
  ServiceLog,
  MissionUser,
  Attachment,
  Code,
  CodeUser,
  UserInfo,
} from '../database/models'
import {
  Mission,
  Role,
  User,
  TutorialImages,
} from '../database/models/RelationMRU'
import { sequelize } from '../database/settings'

const crypto = require('crypto')
var AWS = require('aws-sdk')
AWS.config.update({ region: 'us-east-1'})

const storePhoto = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  try {
    //const Id = event.id;
    const STATUS_COMPLETED = 'completed'
    const STATUS_PENDING = 'pending'
    const STATUS_FAILED = 'failed'
    const STATUS_REOPENED = 'reopened'
    const ATTACHMENT_STATUS_PENDING = 'pending'

    const log = new ServiceLog()
    log.request = JSON.stringify(event)
    log.service = 'missions/photos'
    await log.save()

    const userId = event.sessionUser?.id
    const active = event.queryStringParameters?.active
    const missionId = parseInt(event.pathParameters.mission_id)
    const terms_conditions = event.body.terms_conditions
    const latitude = event.body.lat?.toString()
    const longitude = event.body.long?.toString()

    const photos = event.body.photos
    let today = moment().tz('America/Mexico_City').format('YYYY-MM-DD HH:mm:ss')

    // const user = await User.findOne({
      // where: { id: userId },
      // include: [
        // {
          // model: Role,
          // attributes: ['id', 'name', 'slug'],
        // },
      // ],
    // })

    log.user_id = userId
    log.save()

    if (!terms_conditions) {
      return {
        statusCode: 400,
        body: {
          status: 'failed',
          data: 'No se han aceptado terminos y  condiciones' + terms_conditions,
        },
      }
      // throw new \Exception('No se han aceptado terminos y  condiciones', 400);
    }

    var checkMissionUser = await MissionUser.findOne({
      where: {
        mission_id: missionId,
        user_id: userId,
      },
      include: [
        {
          model: Attachment,
          required: true,
        },
      ],
    })

    if (checkMissionUser !== null && checkMissionUser.status !== 'reopened') {
      return {
        statusCode: 200,
        body: {
          status: 'success',
          data: 'Las fotos se adjuntaron correctamente',
        },
      }
    }

    var cdcData = await CodeUser.findOne({
      where: {
        user_id: userId,
      },
      include: [
        {
          model: Code,
          required: true,
        },
      ],
    })

    var cdcId = cdcData.code.getDataValue('consumption_center_id')

    var checkMissionCdc = await MissionUser.findOne({
      where: {
        mission_id: missionId,
        cdc_id: cdcId,
      },
      include: [
        {
          model: Attachment,
          required: true,
        },
      ],
    })

    //***********************
    // ****  Esto si debe de ir
    //****************************
    if (checkMissionCdc != null && checkMissionCdc.status !== 'reopened') {
      return {
        statusCode: 400,
        body: {
          status: 'failed',
          data: 'Alguien de tu equipo ya envió esta misión, ve a Misiones para ver las nuevas y seguir participando',
        },
        message:
          'Alguien de tu equipo ya envió esta misión, ve a Misiones para ver las nuevas y seguir participando',
      }
    }

    var MissionData = await Mission.findOne({
      where: {
        id: missionId,
      },
    })

    //console.log(MissionData);

    var isTutorial = MissionData.slug == 'tutorial'
    if (MissionData.is_scheduled == 1) {
      var now = moment().tz('America/Mexico_City').format('YYYY-MM-DD HH:mm:ss')
      var isAfter = now > MissionData.end_date

      if (isAfter) {
        return {
          statusCode: 400,
          body: {
            status: "fail",
            data: "'El tiempo de la misión ha expirado'"
          }
        }
      }
    }

    let transaction
    try {
      transaction = await sequelize.transaction()

      var missionUser = await MissionUser.findOne(
        {
          where: {
            mission_id: missionId,
            user_id: userId,
          },
        },
        { transaction }
      )

      let now = moment().tz('America/Mexico_City').format('YYYY-MM-DD HH:mm:ss')

      if (missionUser === null) {
        missionUser = MissionUser.build()
        missionUser.user_id = userId
        missionUser.mission_id = missionId
        missionUser.accept_term_conditions = terms_conditions
        missionUser.status = isTutorial ? STATUS_COMPLETED : STATUS_PENDING
        missionUser.cdc_id = cdcId
        missionUser.created_at = today
        missionUser.updated_at = today
        await missionUser.save({ transaction })
      } else {
        /***************************************************
         ****** ESTAS LINEAS SI DEBEN DE IR *****************
         ****************************************************/
        if (
          missionUser.status == STATUS_FAILED ||
          missionUser.status == STATUS_COMPLETED
        ) {
          return {
            statusCode: 409,
            body: {
              status: 'failed',
              data: 'Esta misión no se encuentra abierta a recibir fotografías',
            }
          }
        }

        missionUser.status = STATUS_PENDING
        await missionUser.save({ transaction })
      }

      // Photos attached must not exceed the mission configuration
      AttachmentData = await MissionUser.findAll(
        {
          where: {
            mission_id: missionId,
            user_id: userId,
          },
          include: [
            {
              model: Attachment,
              where: {
                status: ATTACHMENT_STATUS_PENDING,
              },
            },
          ],
        },
        { transaction }
      )

      totalPendingPhotos = AttachmentData.length
      totalNewPhotos = photos.length
      totalFinal = totalPendingPhotos + totalNewPhotos

      if (totalFinal > MissionData.maxAttachments) {
        remainingPhotos = MissionData.maxAttachments - totalPendingPhotos
        await transaction.rollback()
        return {
          statusCode: 404,
          body: {
            status: 'failed',
            data:
              'Se excedió el número de adjuntos permitidos (' +
              remainingPhotos +
              ' restante(s))',
          },
          message:
            'Se excedió el número de adjuntos permitidos (' +
            remainingPhotos +
            ' restante(s))',
        }
        //throw new \Exception('Se excedió el número de adjuntos permitidos (' . $remainingPhotos . ' restante(s))', 404);
      }

      var s3 = new AWS.S3({ apiVersion: '2006-03-01' })
      for (let row of photos) {
        if (row != '') {
          // Save to s3
          filename =
            'photo-' +
            crypto.randomBytes(16).toString('hex') +
            '-' +
            Math.round(new Date().getTime() / 1000)
          basename = filename + '.jpg'
          extension = 'jpg'
          mime = 'image/jpeg'
          //filePath  = process.env.AWS_DIRECTORY + 'attachments/'  +  basename;
          filePath = 'attachments/' + basename

          var upload = await s3
            .upload({
              Bucket: 'storephotos-tecate',
              Key: filePath,
              Body: row,
            })
            .promise()

          // Save to database
          image = Attachment.build()
          image.mission_user_id = missionUser.id
          image.original = basename
          image.filename = filename
          image.extension = extension
          image.mime = mime
          image.latitude = latitude
          image.longitude = longitude
          //image->status          = $isTutorial ? Attachment::STATUS_PASSED : Attachment::STATUS_PENDING;
          image.status = isTutorial ? STATUS_PASSED : STATUS_PENDING
          await image.save({ transaction })
        }
      }

      if (isTutorial) {
        var userInfo = await UserInfo.findOne(
          {
            where: {
              userId: userId,
            },
          },
          { transaction }
        )

        userInfo.tutorial_completed = 1
        await userInfo.save()

        /* $user                         = User::with('userInfo')->find($userId);
              $userInfo                     = $user->userInfo;
              $userInfo->tutorial_completed = 1;
              $userInfo->save();

              // Copy coordinates from the tutorial photos to the store
              $currentStore = $user->stores()->first();

              if ($currentStore !== null) {
                  $currentStore->latitude  = $latitude;
                  $currentStore->longitude = $longitude;
                  $currentStore->save();
              }

              // Adcinema
              if (env('ADCINEMA_ENABLED', false)) {
                  $response = $this->soapService->getCodes(
                      $user->getKey(),
                      $user->email,
                      $user->username
                  );

                  $smsSuccess = $this->smsService->sendCinemaTickets($userInfo->phone_number, $response);

                  if (!$smsSuccess) {
                      throw new \Exception('No se pudo enviar el SMS', 404);
                  }
              }*/
      }

      //await transaction.rollback();
      await transaction.commit()
      return {
        statusCode: 200,
        body: {
          status: 'success',
          data: 'Las fotos se adjuntaron correctamente',
        },
      }
    } catch (error) {
      console.log('error')
      if (transaction) {
        await transaction.rollback()
      }
      return {
        statusCode: 500,
        body: error,
        message: error.message,
      }
    }
  } catch (error) {
    return {
      statusCode: 500,
      body: error,
      message: error.message,
    }
  }

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
}

export const handler = commonMiddleware(storePhoto)
