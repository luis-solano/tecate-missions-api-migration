import Sequelize from 'sequelize'
import commonMiddleware from '../libs/commonMiddleware'
import { MissionUser, Attachment } from '../database/models'
import {
  Mission,
  Role,
  User,
  TutorialImages,
} from '../database/models/RelationMRU'
import imagesUrls from '../libs/tutorialImagesToArrayUrls'
import simpleHistoryMiddleware from '../libs/simpleHistoryMiddleware'

const Op = Sequelize.Op

const simpleHistory = async (event) => {
  try {
    const userId = event.sessionUser.id
    const active = event.queryStringParameters?.active || 1

    const user = await User.findOne({
      where: { id: userId },
      include: [
        {
          model: Role,
          attributes: ['id', 'name', 'slug'],
        },
      ],
    })

    const now = new Date()
    const monthsEarlier = new Date(now.getFullYear(), now.getMonth(), 1)
    monthsEarlier.setMonth(monthsEarlier.getMonth() - 2)

    let result = await MissionUser.findAll({
      where: {
        user_id: userId,
        created_at: {
          [Op.gte]:
            monthsEarlier.toISOString().split('T')[0] + 'T00:00:00.000Z',
        },
      },
      include: [
        {
          model: Mission,
          include: [
            {
              model: Role,
              where: {
                id: user.roles[0].id,
              },
            },
            {
              model: TutorialImages,
            },
          ],
        },
        {
          model: Attachment,
        },
      ],
      group: ['id'],
      order: [['created_at', 'ASC']]
    })

    const baseUrl = process.env.AWS_URL_IMAGE + '/attachments'

    result = result.map((item) => {
      const mission = imagesUrls(item.mission)
      item.dataValues.mission = mission
      const photos = item.attachments
      const role = item.mission.roles[0]

      delete item.dataValues.attachments
      delete item.dataValues.mission.dataValues.roles
      item.dataValues.comments = []
      item.dataValues.photos = []
      item.dataValues.mission.dataValues.role = role

      if (photos.length) {
        let dateCreatedAtGroupPhoto = new Date(photos[0].created_at)
        item.dataValues.photos = photos.map((photo) => {
          return {
            image: `${baseUrl}/${photo.original}`,
          }
        })
        photos.forEach((photo) => {
          const createdAt = new Date(photo.created_at)

          if (
            !item.dataValues.comments.length ||
            dateCreatedAtGroupPhoto.getTime() !== createdAt.getTime()
          ) {
            if (photo.status === 'pending' || !photo.commentary) {
              return
            }

            dateCreatedAtGroupPhoto = createdAt
            item.dataValues.comments.push({
              status: photo.status,
              comentary: photo.commentary,
              created_at: photo.created_at,
              updated_at: photo.updated_at,
            })
          }
        })
      }

      return item
    })

    return {
      status: 'success',
      data: result,
    }
  } catch (error) {
    return {
      statusCode: 500,
      body: error,
      message: error.message,
    }
  }

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
}

export const handler = commonMiddleware(simpleHistory)
