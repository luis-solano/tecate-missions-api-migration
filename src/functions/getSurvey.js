import commonMiddleware from '../libs/commonMiddleware'
import { Survey, Question, Answer, Segment } from '../database/models'

const getSurvey = async (event) => {
  try {
    const survey = await Survey.findOne({
      where: {
        id: event.pathParameters.survey_id,
      },
      include: [
        {
          model: Question,
          include: [Answer],
        },
        Segment,
      ],
    })

    return {
      status: 'success',
      data: survey,
    }
  } catch (error) {
    return {
      status: 'failed',
      data: {
        message: 'Ocurrió un error inesperado.',
        error: error.message,
        stack: error.stack,
        statusCode: 500,
      },
    }
  }
}

export const handler = commonMiddleware(getSurvey)
