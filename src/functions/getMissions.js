import Sequelize from 'sequelize'
import moment from 'moment-timezone'
import { MissionUser, Code } from '../database/models'
import {
  Mission,
  Role,
  User,
  TutorialImages,
} from '../database/models/RelationMRU'
import imagesUrls from '../libs/tutorialImagesToArrayUrls'
import formatMission from '../libs/formatMission'
import commonMiddleware from '../libs/commonMiddleware'
import getMissionsMiddleware from '../libs/getMissionsMiddleware'

const Op = Sequelize.Op

const getMissions = async (event, context) => {
  try {
    const today = new Date()
    const moment_date = moment().tz('America/Mexico_City').format('YYYY-MM-DD HH:mm:ss')
    const date = moment_date.split(' ')
    const day = today.getDay()
    const user_id = event.sessionUser.id

    const user = await User.findOne({
      where: { id: user_id },
      include: [
        {
          model: Role,
          attributes: ['id', 'name', 'slug'],
        },
        {
          model: Code
        }
      ],
    })

    const missions = await Mission.findAll({
      where: {
        active: parseInt(event.queryStringParameters.active),
        [Op.or]: [{ days: { [Op.like]: `%${day}%` } }, { is_scheduled: 1 }],
        [Op.or]: [
          Sequelize.literal(
            "'" +
              date[0] +
              "' between missions.date_start and missions.date_finish"
          ),
          Sequelize.literal(
            "missions.date_start = '" + date[0] + "' and missions.start_time <= '" + date[1] + "' and missions.end_date >= '" + moment_date + "'"
          )
        ],
      },
      include: [
        {
          model: Role,
          where: { id: user.roles[0].id },
        },
        {
          model: TutorialImages,
        },
        {
          model: MissionUser,
          where: { cdc_id: user.codes[0].consumption_center_id },
          required: false
        }
      ],
      group: ['missions.id'],
      attributes: {
        include: [[Sequelize.fn('COUNT', Sequelize.col('mission_users.cdc_id')), 'cdc_ids']]
      },
      having: Sequelize.literal(`cdc_ids < 1`)
    })

    const mission_reopened = await MissionUser.findAll({
      where: { user_id: user_id, status: 'reopened' },
      include: [
        {
          model: Mission,
          include: [
            {
              model: Role,
              where: { id: user.roles[0].id },
            },
            {
              model: TutorialImages,
            },
          ],
        },
      ],
    })

    const missions_format = missions.map((item) => imagesUrls(item))
    const missions_reopend_format = mission_reopened
      .map((item) => formatMission(item))
      .map((item) => imagesUrls(item))

    missions_reopend_format.forEach((item, i) => {
      missions_format.push(item)
    })

    return {
      statusCode: 200,
      body: {
        status: 'success',
        data: missions_format
      },
    }
  } catch (error) {
    return {
      statusCode: 400,
      body: error,
    }
  }

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
}

export const handler = commonMiddleware(getMissions).use(
  getMissionsMiddleware()
)
