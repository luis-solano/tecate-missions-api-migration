const getMissionsMiddleware = () => {
  const getMissionsMiddlewareBefore = async (request) => {
    const active = parseInt(request.event.queryStringParameters?.active)
    if (active !== 1 && active !== 0) {
      return {
        error: 'Parámetro active con valor requerido 1 o 0',
      }
    }
    if (request.event.queryStringParameters?.type !== 'current') {
      return { error: "Parámetro current con valor requerido 'current'" }
    }
  }

  const getMissionsMiddlewareAfter = async (request) => {}
  const getMissionsMiddlewareError = async (request) => {}

  return {
    // Having descriptive function names will allow for easier tracking of performance bottlenecks using @middy/core/profiler
    before: getMissionsMiddlewareBefore,
    after: getMissionsMiddlewareAfter,
    onError: getMissionsMiddlewareError,
  }
}

export default getMissionsMiddleware
