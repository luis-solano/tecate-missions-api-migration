const jwt = require('jsonwebtoken')
import { User } from '../database/models'

const defaults = {}

const authMiddleware = (opts = {}) => {
  const options = { ...defaults, ...opts }

  const authMiddlewareBefore = async (request) => {
    try {
      const token =
        request.event.headers?.authorization?.split(' ')[1] ||
        request.event.queryStringParameters?.token

      if (!token) {
        return { error: 'Token not provided' }
      }

      let jwtDecoded = jwt.verify(token, process.env.JWT_SECRET)

      request.event.sessionUser = await User.findOne({
        where: {
          id: jwtDecoded.sub,
        },
      })
    } catch (error) {
      return {
        error: 'Token: ' + error.message,
      }
    }
  }
  const authMiddlewareAfter = async (request) => {}
  const authMiddlewareOnError = async (request) => {}

  return {
    // Having descriptive function names will allow for easier tracking of performance bottlenecks using @middy/core/profiler
    before: authMiddlewareBefore,
    after: authMiddlewareAfter,
    onError: authMiddlewareOnError,
  }
}

export default authMiddleware
