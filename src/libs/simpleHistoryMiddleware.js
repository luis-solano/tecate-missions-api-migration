const simpleHistoryMiddleware = () => {
  const simpleHistoryMiddlewareBefore = async (request) => {
    const active = parseInt(request.event.queryStringParameters?.active)
    if (active !== 1 && active !== 0) {
      return {
        error: 'Parámetro active con valor requerido 1 o 0',
      }
    }
  }

  const simpleHistoryMiddlewareAfter = async (request) => {}
  const simpleHistoryMiddlewareError = async (request) => {}

  return {
    // Having descriptive function names will allow for easier tracking of performance bottlenecks using @middy/core/profiler
    before: simpleHistoryMiddlewareBefore,
    after: simpleHistoryMiddlewareAfter,
    onError: simpleHistoryMiddlewareError,
  }
}

export default simpleHistoryMiddleware
