const formatMission = (mission) => {
  const mission_format = mission.mission
  delete mission.dataValues.mission
  mission_format.dataValues.mission_user = mission
  return mission_format
}

export default formatMission
