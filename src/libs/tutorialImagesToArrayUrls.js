const imagesUrls = (mission) => {
  const TYPE_IMAGE_TUTORIAL = 1
  const TYPE_IMAGE_COVERS = 2

  const tutorial_images = mission.tutorial_images
  delete mission.dataValues.tutorial_images

  let tutorial_array = new Array()
  let nameOriginal = ''

  if (tutorial_images !== null) {
    tutorial_images.forEach((item, i) => {
      if (item['type_image_id'] == TYPE_IMAGE_TUTORIAL) {
        nameOriginal = item['image_original']
        let image_name = process.env.AWS_URL_IMAGE + '/tutorials' + '/' + item['image_original']
        let image = tutorial_images.filter(item => item.type_image_id == 1)
        image[0].dataValues = {}
        image[0].dataValues.image = image_name
        mission.dataValues.tutorial_images = image
      } else if (item['type_image_id'] == TYPE_IMAGE_COVERS) {
        let image_name = process.env.AWS_URL_IMAGE + '/cover' + '/' + item['image_original']
        let image = tutorial_images.filter(item => item.type_image_id == 2)
        image[0].dataValues = {}
        image[0].dataValues.image = image_name
        mission.dataValues.cover_images = image
      }

      if (tutorial_images.length === 1) {
        let text = nameOriginal.split('-')
        mission.setDataValue('cover_images', [{image: process.env.AWS_URL_IMAGE + '/cover/c-' + text[1] + '-' + text[2]}]);
      }
    })
  }

  mission.dataValues.role = mission.dataValues.roles
  delete mission.dataValues.roles

  return mission
}

export default imagesUrls
