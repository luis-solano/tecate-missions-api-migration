import moment from 'moment-timezone'
import { Mission, MissionUser, Code } from '../database/models'

const MissionService = {
  /**
   *
   * @param missionId
   * @param userId
   * @param dbTransaction
   * @returns {Promise<string[]>}
   */
  async closeMission(missionId, userId, dbTransaction) {
    if (missionId != null) {
      const acceptedTermsAndConditions = 1 // variable definida en codigo original PHP

      // Validate if user did previously collaborated and if the missions can receive photos
      let mission = await Mission.findOne({ where: { id: missionId } })
      let missionUser = await MissionUser.findOne({
        where: {
          mission_id: missionId,
          user_id: userId,
        },
      })

      if (mission.is_scheduled) {
        if (moment.tz(mission.end_date, 'UTC').diff(moment().tz('UTC')) < 0) {
          throw { message: 'El tiempo de la misión ha expirado', code: 403 }
        }
      }

      let now = moment().tz('America/Mexico_City').format('YYYY-MM-DD HH:mm:ss')

      const code = await Code.findOne({ where: { user_id: userId } })

      if (missionUser == null) {
        missionUser = await MissionUser.create({
          user_id: userId,
          mission_id: missionId,
          accept_term_conditions: acceptedTermsAndConditions,
          status: 'completed',
          created_at: now,
          updated_at: now,
          date_evaluation: now,
          cdc_id: code.consumption_center_id,
        })
      } else {
        if (
          missionUser.status === 'failed' ||
          missionUser.status === 'completed'
        ) {
          throw { message: 'Esta misión ya fue realizada', code: 404 }
        }

        missionUser.status = 'pending'
        missionUser.updated_at = now
        await missionUser.save()
      }

      return [moment().tz('America/Mexico_City').format('YYYY-MM-DD HH:mm:ss')]
    }
  },
}

export default MissionService
