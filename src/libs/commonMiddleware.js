import middy from '@middy/core'
import httpJsonBodyParser from '@middy/http-json-body-parser'
import httpEventNormalizer from '@middy/http-event-normalizer'
import httpResponseSerializer from '@middy/http-response-serializer'
import httpErrorHandler from '@middy/http-error-handler'
import httpHeaderNormalizer from '@middy/http-header-normalizer'
import authMiddleware from './authMiddleware'

export default (handler) =>
  middy(handler).use([
    httpHeaderNormalizer(),
    httpJsonBodyParser(),
    httpEventNormalizer(),
    httpErrorHandler(),
    httpResponseSerializer({
      serializers: [
        {
          regex: /^application\/xml$/,
          serializer: ({ body }) => `<message>${body}</message>`,
        },
        {
          regex: /^application\/json$/,
          serializer: ({ body }) => JSON.stringify(body),
        },
        {
          regex: /^text\/plain$/,
          serializer: ({ body }) => body,
        },
      ],
      default: 'application/json',
    }),
    authMiddleware(),
  ])
