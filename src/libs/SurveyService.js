import moment from 'moment-timezone'
import {
  Answer,
  MissionTransaction,
  Question,
  Survey,
  SurveyEvaluation,
  SurveyEvaluationResponse,
} from '../database/models'
import SurveyEvaluationService from './SurveyEvaluationService'

const SurveyService = {
  /**
   *
   * @param userId
   * @param surveyId
   * @param evaluation
   * @param missionId
   * @param trainingId
   * @param dbTransaction
   * @returns {Promise<string>}
   */
  async addEvaluation(
    userId,
    surveyId,
    evaluation,
    missionId,
    trainingId,
    dbTransaction
  ) {
    const survey = await Survey.findOne({ where: { id: surveyId } })
    const now = moment().tz('America/Mexico_City').format('YYYY-MM-DD HH:mm:ss')

    if (survey == null) {
      throw { message: 'la encuesta no existe', code: 404 }
    }

    const checkSurveyTraining = await SurveyEvaluation.findOne({
      where: {
        user_id: userId,
        mission_id: missionId,
        training_id: trainingId,
      },
    })

    if (checkSurveyTraining !== null) {
      return 'Se envió la evaluación correctamente'
    }

    const checkMissionSent = await MissionTransaction.findOne({
      where: {
        mission_id: missionId,
        user_mission_id: userId,
      },
    })

    if (checkMissionSent !== null) {
      throw { message: 'Esta misión ya fue realizada' }
    }

    const surveyEvaluation = await SurveyEvaluation.create(
      {
        name: survey.name,
        type: 'any',
        points: survey.points,
        user_id: userId,
        survey_id: surveyId,
        mission_id: missionId !== 0 ? missionId : null,
        training_id: trainingId !== 0 ? trainingId : null,
        created_at: now,
        updated_at: now,
      },
      { transaction: dbTransaction }
    )

    for (let i in evaluation) {
      let row = evaluation[i]

      let question = await Question.findOne({ where: { id: row.question } })

      if (question == null) {
        throw { message: 'Una o más preguntas son inválidas', code: 404 }
      }

      if (question.survey_id !== surveyId) {
        throw {
          message: 'Una o más preguntas no corresponden a la encuesta',
          code: 404,
        }
      }

      let answerText, isCorrect
      if (question.type === 'option') {
        let answer = await Answer.findOne({ where: { id: row.answer } })

        if (answer === null) {
          throw { message: 'Una o más respuestas son inválidas', code: 404 }
        }

        answerText = answer.answer
        isCorrect = answer.is_correct
      } else {
        answerText = row.answer
        isCorrect = 1
      }

      await SurveyEvaluationResponse.create(
        {
          question: question.question,
          answer: answerText,
          is_correct: isCorrect,
          survey_evaluation_id: surveyEvaluation.id,
          created_at: now,
          updated_at: now,
        },
        { transaction: dbTransaction }
      )
    }

    await SurveyEvaluationService.evaluateSurvey(
      userId,
      surveyId,
      missionId,
      trainingId,
      dbTransaction
    )

    return 'Se envió la evaluación correctamente'
  },
}

export default SurveyService
