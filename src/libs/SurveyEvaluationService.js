import moment from 'moment-timezone'
import {
  Code,
  Mission,
  Survey,
  UserTransaction,
  ConsumptionCenter,
  MissionTransaction,
  MissionUser,
  MonthMission,
  ManagerProgress,
  User,
  CodeConsumptionCenter,
  AdviserProgress,
  MissionAdviserTransaction,
  MissionFakeTransaction,
} from '../database/models'
import { sequelize } from '../database/settings'
const { Op } = require('sequelize')
import { QueryTypes } from 'sequelize'

const SurveyEvaluationService = {
  /**
   *
   * @param userId
   * @param surveyId
   * @param missionId
   * @param trainingId
   * @param dbTransaction
   * @returns {Promise<void>}
   */
  async evaluateSurvey(userId, surveyId, missionId, trainingId, dbTransaction) {
    const now = moment().tz('America/Mexico_City').format('YYYY-MM-DD HH:mm:ss')
    const survey = await Survey.findOne({ where: { id: surveyId } })
    let sql, evaluation

    if (trainingId !== 0 && missionId === 0) {
      sql =
        'select avg(ser.is_correct)*100 as average, max(se.points) as points ' +
        'from survey_evaluations se ' +
        'join survey_evaluation_responses ser ON se.id = ser.survey_evaluation_id ' +
        'where training_id = ? and survey_id = ? and user_id = ?'

      evaluation = (
        await sequelize.query(sql, {
          replacements: [trainingId, surveyId, userId],
          type: QueryTypes.SELECT,
        })
      )[0]

      // si el resultado tiene el porcentaje minimo de la encuesta
      if (evaluation.average >= survey.percentage) {
        await UserTransaction.create(
          {
            amount: evaluation.points,
            user_id: userId,
            description: 'training: ' + trainingId,
            applied_at: now,
            updated_at: now,
          },
          { transaction: dbTransaction }
        )
      }
    } else {
      sql =
        'select avg(ser.is_correct)*100 as average, max(se.points) as points ' +
        'from survey_evaluations se ' +
        'join survey_evaluation_responses ser ON se.id = ser.survey_evaluation_id ' +
        'where mission_id = ? and survey_id = ? and user_id = ?'

      evaluation = (
        await sequelize.query(sql, {
          replacements: [missionId, surveyId, userId],
          type: QueryTypes.SELECT,
        })
      )[0]

      // si el resultado tiene el porcentaje minimo de la encuesta
      if (evaluation.average >= survey.percentage) {
        let cdcId = (
          await Code.findOne({
            where: { user_id: userId },
          })
        ).consumption_center_id

        let managerId = (
          await ConsumptionCenter.findOne({
            where: { id: cdcId },
          })
        ).user_id

        let isMissionEspecial = (
          await Mission.findOne({
            where: { id: missionId },
          })
        ).especial

        sql =
          'select count(*) as count ' +
          'from mission_transactions mt ' +
          'join user_transactions ut ON ut.id = mt.user_transaction_id ' +
          'where mt.mission_id = ? and ut.user_id = ?'
        let managerIsAlreadyGetPoints = (
          await sequelize.query(sql, {
            replacements: [missionId, userId],
            type: QueryTypes.SELECT,
          })
        )[0].count

        if (managerIsAlreadyGetPoints === 0) {
          let userTransaction = await UserTransaction.create(
            {
              amount: evaluation.points,
              user_id: managerId,
              description: survey.name,
              reference: isMissionEspecial === 1 ? 'extras' : 'survey',
              applied_at: now,
              created_at: now,
            },
            { transaction: dbTransaction }
          )

          await MissionTransaction.create(
            {
              mission_id: missionId,
              user_transaction_id: userTransaction.id,
              user_mission_id: userId,
              created_at: now,
              updated_at: now,
            },
            { transaction: dbTransaction }
          )

          let momentNow = moment().tz('America/Mexico_City')
          sql =
            'select count(*) from mission_user ' +
            'where year(created_at) = ? ' +
            'and month(created_at) = ? ' +
            'and cdc_id = ?'
          let missionCompletedCount = (
            await sequelize.query(sql, {
              replacements: [momentNow.year(), momentNow.month(), cdcId],
              type: QueryTypes.SELECT,
            })
          )[0].count

          let monthMissions = await MonthMission.findOne({
            where: {
              year: momentNow.year(),
              month: momentNow.month(),
            },
          })

          let missionsPercent = 0
          if (monthMissions != null) {
            missionsPercent =
              (100 / monthMissions.missions_number) * missionCompletedCount
          }

          let managerPercent = 0
          if (missionsPercent > 0 && missionsPercent < 20) {
            managerPercent = 10
          } else {
            managerPercent = Math.floor(missionsPercent / 10) * 10
          }

          await ManagerProgress.create(
            {
              user_id: managerId,
              month: momentNow.month(),
              year: momentNow.year(),
              percent: managerPercent,
            },
            { transaction: dbTransaction }
          )
        }

        let adviser = await this.getAdviserOfCdc(cdcId)
        let points = evaluation.points * 1.15

        if (adviser) {
          if (
            await this.willAdviserReceivePoints(cdcId, missionId, dbTransaction)
          ) {
            let userTransaction = await UserTransaction.create(
              {
                user_id: adviser.id,
                amount: points,
                description: survey.name,
                reference: isMissionEspecial === 1 ? 'extras' : 'survey',
                created_at: now,
                applied_at: now,
              },
              { transaction: dbTransaction }
            )

            await MissionAdviserTransaction.create(
              {
                mission_id: missionId,
                user_id: adviser.id,
                user_transaction_id: userTransaction.id,
                created_at: now,
                updated_at: now,
              },
              { transaction: dbTransaction }
            )
          }
        } else {
          let adviserFake = (
            await Code.findOne({
              where: { code: 'ASESORFAKE' },
              include: [User],
            })
          ).user

          let userTransaction = await UserTransaction.create(
            {
              user_id: adviserFake.id,
              amount: points,
              description: survey.name,
              reference: isMissionEspecial === 1 ? 'extras' : 'survey',
              created_at: now,
              applied_at: now,
            },
            { transaction: dbTransaction }
          )

          await MissionFakeTransaction.create(
            {
              mission_id: missionId,
              cdc_id: cdcId,
              user_mission_id: userId,
              points: points,
              user_transaction_id: userTransaction.id,
              created_at: now,
              updated_at: now,
            },
            { transaction: dbTransaction }
          )
        }
      }
    }
  },

  /**
   *
   * @param cdcId
   * @param missionId
   * @param dbTransaction
   * @returns {Promise<boolean>}
   */
  async willAdviserReceivePoints(cdcId, missionId, dbTransaction) {
    let adviserCode = await CodeConsumptionCenter.findOne({
      where: { consumption_center_id: cdcId },
    })

    let adviserCdcs = (
      await CodeConsumptionCenter.findAll({
        where: { code_id: adviserCode.code_id },
      })
    ).map((cdc) => cdc.consumption_center_id)

    let adviserCdcsMissionAlreadyDo = await MissionUser.findAll({
      where: {
        status: 'completed',
        mission_id: missionId,
        cdc_id: {
          [Op.in]: adviserCdcs,
        },
      },
    })

    let adviserAverageCompleteCdcs =
      (100 / adviserCdcs.length) * adviserCdcsMissionAlreadyDo.length
    let adviser = await this.getAdviserOfCdc(cdcId)

    let sql =
      'select count(*) as count ' +
      'from mission_adviser_transactions mat ' +
      'join user_transactions ut ON ut.id = mat.user_transaction_id ' +
      'where mat.mission_id = ? and ut.user_id = ?'

    let adviserIsAlreadyGetPoints = (
      await sequelize.query(sql, {
        replacements: [missionId, adviser.id],
        type: QueryTypes.SELECT,
      })
    )[0].count

    let adviserWillReceivePoints =
      adviserAverageCompleteCdcs >= 80 && adviserIsAlreadyGetPoints === 0

    let adviserProgress = await AdviserProgress.findOne({
      where: { adviser_id: adviser.id, mission_id: missionId },
    })

    if (adviserProgress == null) {
      adviserProgress = await AdviserProgress.create(
        {
          adviser_id: adviser.id,
          mission_id: missionId,
          percent: adviserAverageCompleteCdcs,
        },
        { transaction: dbTransaction }
      )
    } else {
      adviserProgress.percent = adviserAverageCompleteCdcs
      await adviserProgress.save({ transaction: dbTransaction })
    }

    return adviserWillReceivePoints
  },

  /**
   *
   * @param cdcId
   * @returns {Promise<User|null>}
   */
  async getAdviserOfCdc(cdcId) {
    let adviserCode = await CodeConsumptionCenter.findOne({
      where: { consumption_center_id: cdcId },
    })

    if (adviserCode) {
      return (
        await Code.findOne({
          where: { user_id: adviserCode.code_id },
          include: [User],
        })
      ).user
    } else {
      return null
    }
  },
}

export default SurveyEvaluationService
