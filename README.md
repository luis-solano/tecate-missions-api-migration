# Serverless - AWS Node.js

## Estructura del proyecto

```
./
├── src/
│   ├── database/
│   │   └── models/     # ORM Models
│   ├── functions/      # Function handlers
│   └── libs/           # Helpers
├── env.yml
├── package-lock.json
├── package.json
└── serverless.yml      # Serverless config file
```

Este es un proyecto que usa Serverless Framework para su estructura, toda la configuración se encuentra en el archivo `serverless.yml`.

La arquitectura de este proyecto serverless se maneja de la siguiente manera:

* Handlers: Funciones que serán desplegadas en AWS Lambda las cuales fungen como punto de entrada para recibir una solicitud HTTP. Estas funciones _handlers_ están ubicadas en el directorio `./src/functions/` y pueden importar cualquier otro archivo o librería.
* ORM: Permite mapear los datos de la base de datos a objetos dentro del proyecto. La librería usada como ORM es `Sequelieze.js` y su respectiva configuración y mapeo de modelos de datos se encuentra dentro de la carpeta `./src/database/`.
* API Gateway: Puente que servirá para mapear cada una de nuestras funciones handlers a un respectivo endpoint, es decir que cada recurso (por ejemplo A, B y C.) sea alcanzable desde una URL en particular (por ejemplo www.domain.com/recursos/A). Este `gateway` es desplegado en AWS API Gateway automáticamente por Serverless Framework (con la configuración apropiada dentro de `serverless.yml`).

### Variables de entorno

Deberá existir un archivo nombrado `env.yml` que incluya la información para poder conectar con la base de datos con la siguiente estructura:

```yml
dev:
  DEVELOPMENT_ENV_VAR: "VALUE"
prod:
  PRODUCTION_ENV_VAR: "VALUE"
```

## Ejecución y despliegue

### Instalar dependencias

`npm install`

### Ejecutar localmente

Es posible invocar una función handler directamente desde la consola con el siguiente comando:

`sls invoke local --function [functionName]`

Esto no desplegará ningún recurso en AWS, pero es necesario tener configuradas las credenciales para acceso programático a AWS [](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html).

### Desplegar a AWS

`sls deploy --verbose`

La bandera `--verbose` es para mostrar información detallada de la operación. Puede ser omitida.

Desplegar de esta manera deberá retornar un endpoint en pantalla si fué configurado un método http para esta función [](https://www.serverless.com/framework/docs/providers/aws/events/http-api).
