module.exports = {
  otherFunction: otherFunction,
  logResponse: logResponse,
}

function otherFunction(requestParams, context, ee, next) {
  return next()
}
function logResponse(requestParams, response, context, ee, next) {
  console.log('body', response.body)
  console.log('statusCode', response.statusCode)
  return next() // MUST be called for the scenario to continue
}
